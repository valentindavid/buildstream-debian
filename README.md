## Building

You need first to install `devscripts`.

Then go in every directory and run the following:

```
uscan --download-current-version
origtargz
sudo mk-build-deps -i
debuild
```

You can build in that order:
 - `click-man` then install `python3-click-man_0.2.2-1_all.deb`.
 - `sphinx-click` then install `python3-sphinx-click_1.3.0-1_all.deb`
 - `pytest-data-files` then install `python3-pytest-datafiles_1.0-1_all.deb`
 - `grpcio` then install `python3-grpcio_1.15.0-1_amd64.deb`
 - `grpcio-tools` then install `python3-grpcio-tools_1.15.0-1_amd64.deb`
 - `buildstream`
